import React from 'react';

const Welcome = props => {
	const userName = localStorage.getItem('userName');
	return (
		<div name="container">
			<div className="jumbobotron">
				<h1>Bienvenido {userName}!</h1>
			</div>
		</div>
	);
};

export default Welcome;
