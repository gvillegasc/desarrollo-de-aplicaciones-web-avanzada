var http = require('http'),
    fs = require('fs');
    dateFormat = require('dateformat');

var parametros = [],
valores = [],
arreglo_parametros = [];

http.createServer(function(req, res) {
  fs.readFile('./form.html', function(err, html) {
    var html_string = html.toString();

    var formatoextenso = new Date();
    var formatoGMT = dateFormat("longTime"); 

    res.writeHead(200,{'Content-type':'text/html'});
    html_string = html_string.replace('{formato0}',formatoextenso);
    html_string = html_string.replace('{formato1}',formatoGMT);
    res.write(html_string);
    res.end();

  });
}).listen(8080);
