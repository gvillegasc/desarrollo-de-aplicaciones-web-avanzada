import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Home from './views/Home';
import Details from './views/Details';
import Login from './views/Login';

class App extends Component {
  render() {
    return (<BrowserRouter>
      <div>
        <h1>Sitio de Películas</h1>
        <Switch>
          <Route path='/' exact component={Home}></Route>
          <Route path='/details' component={Details}></Route>
          <Route path='/login' component={Login}></Route>
        </Switch>
      </div>
    </BrowserRouter>);
  }
}

export default App;
